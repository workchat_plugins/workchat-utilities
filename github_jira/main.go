package main

import (
	"os"

	"gitlab.com/w1572/workchat-utilities/github_jira/cmd"
)

func main() {
	if err := cmd.Run(os.Args[1:]); err != nil {
		os.Exit(1)
	}
}
