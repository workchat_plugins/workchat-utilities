package main

import (
	"os"

	"gitlab.com/w1572/workchat-utilities/mmgotool/commands"
)

func main() {
	if err := commands.Run(os.Args[1:]); err != nil {
		os.Exit(1)
	}
}
