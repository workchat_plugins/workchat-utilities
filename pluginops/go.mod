module gitlab.com/w1572/workchat-utilities/pluginops

go 1.12

require (
	github.com/blang/semver/v4 v4.0.0
	github.com/go-git/go-git/v5 v5.1.0
	github.com/google/go-github/v32 v32.1.0
	github.com/manifoldco/promptui v0.7.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.4.0
	github.com/stretchr/testify v1.7.1
	gitlab.com/w1572/backend v0.0.0-20220609073129-45bd0c2e991b // indirect
	golang.org/x/oauth2 v0.0.0-20220223155221-ee480838109b
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
